//
// Created by Simone Belingheri on 06/03/2021.
//

#ifndef INC_3DCO_COMESH_H
#define INC_3DCO_COMESH_H


class coMesh {
public:
    unsigned int m_numVertices;
    unsigned int m_numFaces;
};


#endif //INC_3DCO_COMESH_H
